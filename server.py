import subprocess
import socket
from flask import Flask, request

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def sol():
    if request.method=="GET":
        # get ip address of the server
        ip_address = socket.gethostbyname(socket.gethostname())
        return ip_address
    else:
        # open subprocess that runs stress_cpu.py
        subprocess.Popen(["python3", "stress_cpu.py"])
        return ""

if __name__=="__main__":
    app.run(port=80, host="0.0.0.0")
